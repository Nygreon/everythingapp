//
//  ContainerController.swift
//  EverythingApp
//
//  Created by Pontus Nygren on 2019-06-25.
//  Copyright © 2019 Ponyindustries. All rights reserved.
//

import Foundation
import UIKit

class ContainerController: UIViewController {

    //MARK:- Props

    var menuController: MenuController!
    var centerController: UIViewController!
    var isExpanded = false

    //MARK:- Init

    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeController()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

    override var prefersStatusBarHidden: Bool {
        return isExpanded
    }
    //MARK:- Handlers

    func configureHomeController() {
        let homeController = HomeController()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)

        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }

    func configureMenuController() {
        if menuController == nil {
            menuController = MenuController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
            print("did add menu")
        }
    }

    func animatePanel(shouldExpand: Bool, menuOption: MenuOption?) {

        if shouldExpand {
            // show menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)

        } else {
            // hide menu

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }) { (_) in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }

        animateStatusBar()
    }

    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }


    func didSelectMenuOption(menuOption: MenuOption) {
        switch menuOption {
        case .Profile:
            DispatchQueue.main.async {
                self.present(ProfilePageController(), animated: true, completion: nil)
            }
        case .Inbox:
            print("Inbox")
        case .Notifications:
            print("Notific")
        case .Settings:
            print("Settings")
        }
    }
}

extension ContainerController: HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?) {
        if !isExpanded {
            configureMenuController()
        }

        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded, menuOption:  menuOption)
    }
}

