//
//  TabBarController.swift
//  EverythingApp
//
//  Created by Pontus Nygren on 2019-06-27.
//  Copyright © 2019 Ponyindustries. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        tabBar.barTintColor = .darkGray
        tabBar.tintColor = .white
        let firstViewController = TableViewController()

        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)

        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()


        let secondViewController = CollectionViewController(collectionViewLayout: layout)

        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)

        let tabBarList = [firstViewController, secondViewController]

        viewControllers = tabBarList
    }
}
