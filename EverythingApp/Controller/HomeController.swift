//
//  HomeController.swift
//  EverythingApp
//
//  Created by Pontus Nygren on 2019-06-25.
//  Copyright © 2019 Ponyindustries. All rights reserved.
//

import Foundation
import UIKit

class HomeController: UIViewController {

    //MARK:- Props
    var delegate: HomeControllerDelegate?
    //MARK:- Init

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        configureNavBar()
        navigationController?.addChild(TabBarController())
    }
    //MARK:- Handlers


    func configureNavBar() {
        navigationController?.navigationBar.barTintColor = .darkGray
        navigationController?.navigationBar.barStyle = .black

        navigationItem.title = "Everything app"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
    }

    @objc func handleMenuToggle() {
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
}
