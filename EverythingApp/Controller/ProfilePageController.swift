//
//  ProfilePageVCViewController.swift
//  EverythingApp
//
//  Created by Pontus Nygren on 2019-06-27.
//  Copyright © 2019 Ponyindustries. All rights reserved.
//

import UIKit

class ProfilePageController: UIViewController {

    //MARK:- Props


    //MARK:- View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
//        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        setUpUI()
    }

    //MARK:- Handlers

    func setUpUI() {

        imageviewHolder.translatesAutoresizingMaskIntoConstraints = false
        profilePictureImageView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(mainStackView)
        mainStackView.addArrangedSubview(topStackView)
        topStackView.addArrangedSubview(imageviewHolder)
        topStackView.addArrangedSubview(yellow)
        topStackView.addArrangedSubview(bottomTopStackView)
        bottomTopStackView.addArrangedSubview(green)
        bottomTopStackView.addArrangedSubview(gray)
        bottomTopStackView.addArrangedSubview(black)
        mainStackView.addArrangedSubview(bottomStackView)
        bottomStackView.addArrangedSubview(darkGray)
        imageviewHolder.addSubview(profilePictureImageView)
//
        mainStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        mainStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mainStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true

        imageviewHolder.heightAnchor.constraint(equalToConstant: 220).isActive = true
        yellow.heightAnchor.constraint(equalToConstant: 72).isActive = true
        bottomTopStackView.heightAnchor.constraint(equalToConstant: 64).isActive = true
//
//        //  random props to be removed
        imageviewHolder.backgroundColor = .blue
        profilePictureImageView.backgroundColor = .green

//        let image: UIImage = UIImage(named: "profile")!


//        profilePictureImageView.image = image
//        profilePictureImageView.maskCircle()
        profilePictureImageView.centerXAnchor.constraint(equalTo: imageviewHolder.centerXAnchor).isActive = true
        profilePictureImageView.centerYAnchor.constraint(equalTo: imageviewHolder.centerYAnchor).isActive = true
        profilePictureImageView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        profilePictureImageView.widthAnchor.constraint(equalToConstant: 160).isActive = true


    }

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.width / 2
            self.profilePictureImageView.layer.masksToBounds = true
        }
    }


    //MARK:- Components

    let mainStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical

        return sv
    }()

    let topStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical
        sv.distribution = .fillProportionally


        return sv
    }()

    let bottomStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical
        //        sv.distribution = .fillEqually

        return sv
    }()

    let bottomTopStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .horizontal
        sv.distribution = .fillEqually

        return sv
    }()

    let blue: UIView = {
        let view = UIView()
        view.backgroundColor = .blue
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }()
    let green: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let yellow: UIView = {
        let view = UIView()
        view.backgroundColor = .yellow
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let gray: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let black: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let darkGray: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()


    let imageviewHolder = UIView()

    let profilePictureImageView: UIImageView = {
        let iv = UIImageView()
//        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = UIImage(named: "profile")
        return iv
    }()

    let label = UILabel()


}

extension UIImageView {
    public func maskCircle() {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true

        // make square(* must to make circle),
        // resize(reduce the kilobyte) and
        // fix rotation.
    }
}
