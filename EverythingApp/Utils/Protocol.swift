//
//  Protocol.swift
//  EverythingApp
//
//  Created by Pontus Nygren on 2019-06-26.
//  Copyright © 2019 Ponyindustries. All rights reserved.
//

import Foundation

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?)
}
